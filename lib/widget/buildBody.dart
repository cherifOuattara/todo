

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todoapps/blocs/body_todos/body_todos.dart';
import 'package:todoapps/blocs/todos/todos_bloc.dart';
import 'package:todoapps/blocs/todos/todos_event.dart';
import 'package:todoapps/screens/details_todo_screen.dart';
import 'package:todoapps/widget/todo_item.dart';

import 'delete_snack.dart';

class buildsBody extends StatelessWidget {
  buildsBody({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<BodyTodosBloc, BodyTodosState>(
      builder: (context, state) {
        if (state is BodyTodosLoadInProgress) {
          return Center(
             child: CircularProgressIndicator());
          //return LoadingIndicator(key: ArchSampleKeys.todosLoading);
        } else if (state is BodyTodosLoadSuccess) {
          final todos = state.bodyTodos;
          return ListView.builder(
            itemCount: todos.length,
            itemBuilder: (BuildContext context, int index) {
              final todo = todos[index];
              return TodoItem(
                todo: todo,
                onDismissed: (direction) {
                  BlocProvider.of<TodosBloc>(context).add(TodoDeleted(todo));
                },
                onTap: () async {
                  final removedTodo = await Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) {
                      return DetailsScreen(id: todo.id);
                    }),
                  );
                  if (removedTodo != null) {
                    Scaffold.of(context).showSnackBar(DeleteTodoSnackBar(
                      todo: todo,
                      onUndo: () => BlocProvider.of<TodosBloc>(context)
                          .add(TodoAdded(todo)),
                    ));
                  }
                },
                onCheckboxChanged: (_) {
                  BlocProvider.of<TodosBloc>(context).add(
                    TodoUpdated(todo.copyWith(complete: !todo.complete)),
                  );
                },
              );
            },
          );
        } else {
          return Container();
        }
      },
    );
  }
}