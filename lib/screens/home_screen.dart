import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todoapps/blocs/blocs.dart';
import 'package:todoapps/blocs/body_todos/body_todos_bloc.dart';
import 'package:todoapps/blocs/todos/todos_bloc.dart';
import 'package:todoapps/localstorage.dart';
import 'package:todoapps/models/todo.dart';
import 'package:todoapps/widget/buildBody.dart';
import 'package:todos_repository_simple/todos_repository_simple.dart';


import '../my_routes.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  LocalStorage localStorage = new LocalStorage();

  var result;
  Map data;
  static List<Todo> list = List<Todo>();
  Todo item;
  TodosBloc todosBloc;
  StreamSubscription todosSubscription;


  @override
  Widget build(BuildContext context) {
     return Scaffold(
          appBar: AppBar(
            title: Text('Home'),
          ),
          body:  buildsBody(),
          floatingActionButton: FloatingActionButton(
            onPressed: () async{ Navigator.pushNamed(context, Routes.addTodo);

            },
            child: Icon(Icons.add),
          ),

        );
      //}
    //);



  }



}