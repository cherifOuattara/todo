import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todoapps/blocs/todos/todos_event.dart';
import 'package:todoapps/blocs/todos/todos_state.dart';
import 'package:todoapps/models/todo.dart';
import 'package:todos_repository_simple/todos_repository_simple.dart';


class TodosBloc extends Bloc<TodosEvent, TodosState> {

  final TodosRepositoryFlutter todosRepository;

  TodosBloc({@required this.todosRepository});

  @override
  TodosState get initialState => TodosLoadInProgress();

  @override
  Stream<TodosState> mapEventToState(TodosEvent event) async* {
    if (event is TodosLoaded) {
      yield* _mapTodosLoadedToState();
    } else if (event is TodoAdded) {
      yield* _mapTodoAddedToState(event);
    }else if (event is TodoUpdated) {
      yield* _mapTodoUpdatedToState(event);
    }else if (event is TodoDeleted) {
      yield* _mapTodoDeletedToState(event);
    }
  }


  Stream<TodosState> _mapTodosLoadedToState() async* {
    print('_mapTodosLoadedToState');
    try {
      final todos = await this.todosRepository.loadTodos();
      yield TodosLoadSuccess(
        todos.map(Todo.fromEntity).toList(),
      );
    } catch (_) {
      yield TodosLoadFailure();
    }
  }

  Stream<TodosState> _mapTodoDeletedToState(TodoDeleted event) async* {
    if (state is TodosLoadSuccess) {
      final todos = (state as TodosLoadSuccess)
          .todos
          .where((todo) => todo.id != event.todo.id)
          .toList();
      yield TodosLoadSuccess(todos);
      saveTodos(todos);
    }
  }

  Stream<TodosState> _mapTodoUpdatedToState(TodoUpdated event) async* {
    if (state is TodosLoadSuccess) {
      final List<Todo> updatedTodos =
      (state as TodosLoadSuccess).todos.map((todo) {
        return todo.id == event.todo.id ? event.todo : todo;
      }).toList();
      yield TodosLoadSuccess(updatedTodos);
      saveTodos(updatedTodos);
    }
  }

  Stream<TodosState> _mapTodoAddedToState(TodoAdded event) async* {
    if (state is TodosLoadSuccess) {
      final List<Todo> todos =
      List.from((state as TodosLoadSuccess).todos)..add(event.todo);
      yield TodosLoadSuccess(todos);
      saveTodos(todos);
    }
  }

  Future saveTodos(List<Todo> todos) {
    return todosRepository.saveTodos(
      todos.map((todo) => todo.toEntity()).toList(),
    );
  }


}