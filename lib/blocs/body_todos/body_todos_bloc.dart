import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todoapps/blocs/body_todos/body_todos_event.dart';
import 'package:todoapps/blocs/todos/todos_bloc.dart';
import 'package:todoapps/blocs/todos/todos_state.dart';
import 'package:todoapps/models/todo.dart';

import 'body_todos_state.dart';

class BodyTodosBloc extends Bloc<BodyTodosEvent, BodyTodosState> {
  final TodosBloc todosBloc;
  StreamSubscription todosSubscription;

  BodyTodosBloc({@required this.todosBloc}) {
    todosSubscription = todosBloc.listen((state) {
      if (state is TodosLoadSuccess) {
        add(TodosUpdated((todosBloc.state as TodosLoadSuccess).todos));
      }
    });
  }

  @override
  BodyTodosState get initialState {
    return todosBloc.state is TodosLoadSuccess
        ? BodyTodosLoadSuccess(
      (todosBloc.state as TodosLoadSuccess).todos
    )
        : BodyTodosLoadInProgress();
  }

  @override
  Stream<BodyTodosState> mapEventToState(BodyTodosEvent event) async* {
    if (event is BodyUpdated) {
      yield* _mapBodyUpdatedToState(event);
    } else if (event is TodosUpdated) {
      yield* _mapTodosUpdatedToState(event);
    }
  }

  Stream<BodyTodosState> _mapBodyUpdatedToState(
      BodyUpdated event,
      ) async* {
    if (todosBloc.state is TodosLoadSuccess) {
      yield BodyTodosLoadSuccess(
        _mapTodosToBodyTodos(
          (todosBloc.state as TodosLoadSuccess).todos,
        ),
      );
    }
  }

  Stream<BodyTodosState> _mapTodosUpdatedToState(
      TodosUpdated event,
      ) async* {
    yield BodyTodosLoadSuccess(
      _mapTodosToBodyTodos(
        (todosBloc.state as TodosLoadSuccess).todos,
      ),
    );
  }

  List<Todo> _mapTodosToBodyTodos(
      List<Todo> todos) {
    return todos.where((todo) {
        return true;
    }).toList();
  }

  @override
  Future<void> close() {
    todosSubscription.cancel();
    return super.close();
  }
}