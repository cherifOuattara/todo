import 'package:equatable/equatable.dart';
import 'package:todoapps/models/todo.dart';

abstract class BodyTodosState extends Equatable {
  const BodyTodosState();

  @override
  List<Object> get props => [];
}

class BodyTodosLoadInProgress extends BodyTodosState {}

class BodyTodosLoadSuccess extends BodyTodosState {
  final List<Todo> bodyTodos;

  const BodyTodosLoadSuccess(
      this.bodyTodos,
      );

  @override
  List<Object> get props => [bodyTodos];

  @override
  String toString() {
    return 'BodyTodosLoadSuccess { bodyTodos: $bodyTodos }';
  }
}