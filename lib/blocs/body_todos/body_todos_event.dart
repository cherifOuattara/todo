import 'package:equatable/equatable.dart';
import 'package:todoapps/models/todo.dart';

abstract class BodyTodosEvent extends Equatable {
  const BodyTodosEvent();
}

class BodyUpdated extends BodyTodosEvent {

  const BodyUpdated();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'FilterUpdated';
}

class TodosUpdated extends BodyTodosEvent {
  final List<Todo> todos;

  const TodosUpdated(this.todos);

  @override
  List<Object> get props => [todos];

  @override
  String toString() => 'TodosUpdated { todos: $todos }';
}