import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todoapps/blocs/body_todos/body_todos.dart';
import 'package:todoapps/screens/add_edit_screen.dart';

import 'package:todos_repository_simple/todos_repository_simple.dart';
import 'blocs/simple_bloc_delegate.dart';
import 'blocs/todos/todos_bloc.dart';
import 'blocs/todos/todos_event.dart';
import 'models/todo.dart';
import 'my_routes.dart';
import 'screens/home_screen.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(
    BlocProvider(
      create: (context) => TodosBloc(
        todosRepository: const TodosRepositoryFlutter(
          fileStorage: const FileStorage(
            '__data__',
            getApplicationDocumentsDirectory,
          ),
        )
      )..add(TodosLoaded()),
      child: TodosApp(),
    ),
  );
}

class TodosApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "AppTodo",
      theme: ThemeData(
        primaryColor: Colors.deepOrange,
        accentColor: Colors.deepOrangeAccent,
        appBarTheme: AppBarTheme(
          color: Colors.black,
          iconTheme: IconThemeData(color: Colors.white),
          textTheme: TextTheme(),
          actionsIconTheme: IconThemeData(color: Colors.white),
        ),
        primaryIconTheme: IconThemeData(color: Colors.white),
      ),

      routes: {

        Routes.home: (context) {

          return MultiBlocProvider(
            providers: [
              BlocProvider<BodyTodosBloc>(
                create: (context) => BodyTodosBloc(
                  todosBloc: BlocProvider.of<TodosBloc>(context),
                ),
              ),

            ],
            child: HomeScreen(),
          );

        },
        Routes.addTodo: (context) {
          return AddEditScreen(
             onSave: (task, note) {
               print(task.toString());
              BlocProvider.of<TodosBloc>(context).add(
                TodoAdded(Todo(task, note: note)),
              );
            },
            isEditing: false,
          );
        },
      },
    );
  }
}
