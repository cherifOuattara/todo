
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  String destination;
  var value;
  LocalStorage({
    this.destination,
    this.value
  });
  Future<String> get localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }
  Future<File> _localFile(String destination) async {
    final path = await localPath;
    return File('$path/$destination.txt');
  }
  Future<File> writeLocal(String destination, value) async {
    final file = await _localFile(destination);
    return file.writeAsString('$value');
  }
  Future<String> readLocal(String destination) async {
    try {
      final file = await _localFile(destination);
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      return null;
    }
  }
  Future whiteSharedLocal(String key, bool value) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.setBool(key, value);
    } catch (e) {
      return null;
    }
  }
  Future readSharedLocal(String value) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      return prefs.getBool(value);
    } catch (e) {
      return null;
    }
  }
}