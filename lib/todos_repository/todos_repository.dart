

import 'dart:async';

import 'package:todoapps/models/todo.dart';


abstract class TodosRepository {

  Future<List<Todo>> loadTodos();

  Future saveTodos(Todo todos);

  Future clickTodos(Todo todos);



}


